<?php

namespace Drupal\entity_delete_op\Plugin\Action\Derivative;

use Drupal\entity_delete_op\EntityDeletableInterface;
use Drupal\Core\Action\Plugin\Action\Derivative\EntityActionDeriverBase;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Provides an action deriver for finding entity types supported.
 */
class PurgeEntityActionDeriver extends EntityActionDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected function isApplicable(EntityTypeInterface $entity_type) {
    $entity_delete_op = $entity_type->get('entity_delete_op');
    if ($entity_delete_op && $entity_delete_op !== 'no-purge' && $entity_type->entityClassImplements(EntityDeletableInterface::class)) {
      return TRUE;
    }
    return FALSE;
  }

}
