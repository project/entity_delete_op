<?php

namespace Drupal\entity_delete_op\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\entity_delete_op\DeleteManagerInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines a route subscriber for altering entity routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The delete manager.
   *
   * @var \Drupal\entity_delete_op\DeleteManagerInterface
   */
  protected $deleteManager;

  /**
   * Creates a new instance of RouteSubscriber.
   *
   * @param \Drupal\entity_delete_op\DeleteManagerInterface $delete_manager
   *   The delete manager.
   */
  public function __construct(DeleteManagerInterface $delete_manager) {
    $this->deleteManager = $delete_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */
    $entity_types = $this->deleteManager->getSupportedEntityTypes();
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if (!$entity_type->get('entity_delete_op')) {
        continue;
      }

      // Alter the assumed traditional entity delete route so that it is updated
      // to utilize our delete confirmation form instead. This will provide
      // coverage to assist in ensuring the operation is simply marking the
      // entity as deleted instead of purging from persistent storage.
      $route = $collection->get("entity.$entity_type_id.delete_form");
      if (!empty($route)) {
        $defaults = $route->getDefaults();
        // Remove the assumed `_entity_form` property in place of our form.
        unset($defaults['_entity_form']);
        $defaults['_form'] = '\Drupal\entity_delete_op\Form\DeleteForm';
        $defaults['_entity_delete_op'] = TRUE;
        $route->setDefaults($defaults);
        $route->setOption('parameters', [
          $entity_type_id => [
            'type' => 'entity:' . $entity_type_id
          ],
        ]);
      }
    }
  }

}
