<?php

namespace Drupal\entity_delete_op\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\PermissionHandlerInterface;
use Drupal\views\Plugin\views\filter\BooleanOperator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filters entities by deleted.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsFilter("entity_deleted_op_filter")
 */
class EntityDeleteOp extends BooleanOperator {

  /**
   * The permissions handler service.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $userPermissions;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user.permissions'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PermissionHandlerInterface $user_permissions, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userPermissions = $user_permissions;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['permission']['default'] = '';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    if (!empty($form['expose'])) {

      $options = [];
      foreach ($this->userPermissions->getPermissions() as $permission => $meta) {
        $options[$permission] = $meta['title'];
      }

      $form['permission'] = [
        '#type' => 'select',
        '#title' => $this->t('Permission'),
        '#description' => $this->t('Restrict access to the deleted exposed filter.'),
        '#options' => $options,
        '#empty_option' => $this->t('Not restricted'),
        '#default_value' => $this->options['permission'],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitExposeForm($form, FormStateInterface $form_state) {
    parent::submitExposeForm($form, $form_state);
    $this->options['permission'] = $form_state->getValue(['options', 'permission'], '');
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state) {
    parent::buildExposedForm($form, $form_state);

    $permission = $this->options['permission'];
    if ($permission) {
      $form['deleted']['#access'] = $this->currentUser->hasPermission($permission);
    }
  }

}
