<?php

namespace Drupal\entity_delete_op\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use League\Container\Exception\NotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_delete_op\DeleteManagerInterface;

/**
 * The purge entity double confirm form.
 */
class DoubleConfirmForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\entity_delete_op\DeleteManagerInterface definition.
   *
   * @var \Drupal\entity_delete_op\DeleteManagerInterface
   */
  protected $deleteManager;

  /**
   * Entity Delete Op settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The entity to purge.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Constructs a new DoubleConfirmForm object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DeleteManagerInterface $entity_delete_op_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->deleteManager = $entity_delete_op_manager;
    $this->config = $this->config('entity_delete_op.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_delete_op.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'double_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL, $entity_id = NULL) {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $this->entity = $storage->load($entity_id);

    if (empty($this->entity)) {
      throw new NotFoundException($this->t('The entity with ID @id was not found.', ['@id' => $entity_id]));
    }

    if (!$this->entity->getEntityType()->get('entity_delete_op')) {
      throw new NotFoundException($this->t('The entity with ID @id is not supported.', ['@id' => $entity_id]));
    }

    $form['question'] = [
      '#type' => 'item',
      '#markup' => $this->t('Are you really sure you want to <strong>permanently remove</strong> @label?', [
        '@label' => $this->entity->label(),
      ]),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->config->get('purge_label') ?: $this->t('Purge'),
      '#attributes' => [
        'class' => [
          'button',
          'button--primary',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->deleteManager->purge($this->entity);
    $this->messenger()
      ->addMessage($this->t('The entity "%label" has been @action_label.', [
        '%label' => $this->entity->label(),
        '@action_label' => $this->config->get('purge_label_past') ?: $this->t('purged'),
      ]));
    $form_state->setRedirect('<front>');
  }

}
