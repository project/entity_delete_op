<?php

namespace Drupal\entity_delete_op\Plugin\Menu;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Dynamic entity_delete_op local task.
 *
 * Changes the Delete local task to Restore when the entity has been deleted.
 *
 * @package Drupal\entity_delete_op\Plugin\Menu
 */
class EntityDeleteOpLocalTask extends LocalTaskDefault {
  use StringTranslationTrait;

  /**
   * Whether this entity has been flagged deleted.
   *
   * @var bool
   */
  protected $isDeleted;

  /**
   * The entity being viewed.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = \Drupal::routeMatch();
    $this->isDeleted = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {

    foreach ($this->routeMatch->getParameters() as $entity) {
      if ($entity instanceof EntityInterface) {
        $this->entity = $entity;
        // Break out of the loop on first entity found.
        break;
      }
    }

    if ($this->entity && $this->entity->isDeleted()) {
      $this->isDeleted = TRUE;
      return 'entity_delete_op.restore_form';
    }

    return $this->getPluginDefinition()['route_name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(Request $request = NULL) {
    return $this->isDeleted ? $this->t('Restore') : $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    if (!$this->entity || !$this->isDeleted) {
      return parent::getRouteParameters($route_match);
    }

    $route_parameters = [
      'entity_type_id' => $this->entity->getEntityTypeId(),
      'entity_id' => $this->entity->id(),
    ];

    return $route_parameters;
  }

}
