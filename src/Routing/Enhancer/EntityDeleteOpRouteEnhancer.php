<?php

namespace Drupal\entity_delete_op\Routing\Enhancer;

use Drupal\Core\Routing\EnhancerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Entity Delete Op route enhancer.
 */
class EntityDeleteOpRouteEnhancer implements EnhancerInterface {

  /**
   * {@inheritdoc}
   */
  public function enhance(array $defaults, Request $request) {
    if (!isset($defaults['_entity_delete_op'])) {
      return $defaults;
    }

    // Provide the required parameters to the entity_delete_op form.
    foreach ($defaults['_raw_variables']->all() as $entity_type_id => $entity_id) {
      $defaults['entity_type_id'] = $entity_type_id;
      $defaults['entity_id'] = $entity_id;
    }

    return $defaults;
  }

}
