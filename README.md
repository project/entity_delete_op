## Entity delete operations

Provides the capabilities of performing "soft" delete (archive) operations and restoration in Drupal 8.

### License

This Drupal module is licensed under the [GNU General Public License](./LICENSE.md) version 2.

### Setup

This is an API module that must be implemented by entity types that wish to support it.

* Add an `entity_delete_op` to the entity annotation
  * `entity_delete_op = true` The default behaviour. Delete, Purge, and restore enabled.
  * `entity_delete_op = no-purge` Disable purging on this entity.
  * `entity_delete_op = double-confirmation` Ask twice before purging forever.
* Entity class should implement `EntityDeletableInterface` and `use EntityDeletableTrait`
* If entity types declare their own delete permissions, users must be granted that permission as well as the entity_delete_op permission for the entity type.
* Listing views will need to be updated to filter the "soft deleted" items. You can specify a permission in the filter config that restricts access to the exposed filter.
