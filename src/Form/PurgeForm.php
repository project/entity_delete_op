<?php

namespace Drupal\entity_delete_op\Form;

use Drupal\entity_delete_op\DeleteManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use League\Container\Exception\NotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The purge entity form.
 */
class PurgeForm extends ConfirmFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The delete manager.
   *
   * @var \Drupal\entity_delete_op\DeleteManagerInterface
   */
  protected $deleteManager;

  /**
   * The Entity Delete Op settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The entity to purge.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * PurgeForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\entity_delete_op\DeleteManagerInterface $delete_manager
   *   The delete manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DeleteManagerInterface $delete_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->deleteManager = $delete_manager;
    $this->config = $this->config('entity_delete_op.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_delete_op.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_delete_op.purge_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t($this->config->get('purge_label'));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('<front>');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to @action_label "@label"?', [
      '@action_label' => $this->config->get('purge_label') ?: $this->t('purge'),
      '@label' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This action cannot be undone and @plural_purge_label the entity from the database.', [
      '@plural_purge_label' => $this->config->get('purge_label_future') ?: $this->t('purges'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL, $entity_id = NULL) {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $this->entity = $storage->load($entity_id);

    if (empty($this->entity)) {
      throw new NotFoundException($this->t('The entity with ID @id was not found.', ['@id' => $entity_id]));
    }

    if (!$this->entity->getEntityType()->get('entity_delete_op')) {
      throw new NotFoundException($this->t('The entity with ID @id is not supported.', ['@id' => $entity_id]));
    }

    $form = parent::buildForm($form, $form_state);

    if ($this->entity->getEntityType()
      ->get('entity_delete_op') == 'double-confirmation') {
      $form['actions']['submit'] = [
        '#type' => 'link',
        '#title' => $this->config->get('purge_label') ?: $this->t('Purge'),
        '#url' => Url::fromRoute('entity_delete_op.double_confirm_form', [
          'entity_type_id' => $entity_type_id,
          'entity_id' => $entity_id,
        ]),
        '#attributes' => [
          'class' => [
            'use-ajax',
            'button',
            'button--primary',
          ],
          'data-dialog-type' => 'modal',
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->deleteManager->purge($this->entity);
    $this->messenger()
      ->addMessage($this->t('The entity "%label" has been @action_label.', [
        '%label' => $this->entity->label(),
        '@action_label' => $this->config->get('purge_label_past') ?: $this->t('purged'),
      ]));
    $form_state->setRedirect('<front>');
  }

}
