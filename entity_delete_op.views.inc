<?php

/**
 * @file
 * Contains entity_delete_op.views.inc.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data_alter().
 */
function entity_delete_op_views_data_alter(array &$data) {

  $entity_delete_op = Drupal::service('entity_delete_op.manager');

  foreach ($entity_delete_op->getSupportedEntityTypes() as $id => $type) {

    /* @var \Drupal\Core\Entity\ContentEntityType $type */
    $table = $type->getDataTable();
    if (empty($data[$table]['deleted'])) {
      // Bundled entities.
      $table = $type->getBaseTable();
      if (empty($data[$table]['deleted'])) {
        continue;
      }
    }

    $data[$table]['deleted']['filter'] = [
      'title' => new TranslatableMarkup('Deleted'),
      'help' => new TranslatableMarkup('Filter @entity_type by deleted. (optional) Restricted by permission', [
        '@entity_type' => $type->getLabel(),
      ]),
      'field' => 'deleted',
      'id' => 'entity_deleted_op_filter',
    ];
  }
}
